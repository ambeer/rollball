﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform player_pos;
    Vector3 difference = Vector3.zero;

	// Use this for initialization
	void Start () {
        difference = transform.position - player_pos.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = player_pos.position;
	}
}
