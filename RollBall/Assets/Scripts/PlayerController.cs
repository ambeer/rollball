﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float jumpForce = Input.GetAxis("Jump");

		GetComponent<Rigidbody>().AddForce(new Vector3(horizontal, 0, vertical));
	}

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Pickup"))
        {

            Destroy(other);
        }
    }
        
    }
}
